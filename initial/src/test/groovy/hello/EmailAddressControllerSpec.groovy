package hello

import groovy.json.JsonBuilder
import hello.helper.MockMvcHelper
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class EmailAddressControllerSpec extends Specification {

    MockMvc mockMvc

    PersonRepository repository

    def setup() {
        repository = Mock(PersonRepository)
        mockMvc = MockMvcBuilders.standaloneSetup(new PersonController(repository: repository)).build()
    }

    def 'c.r.u.d person and email address'() {
        def INITIAL_FIRST_NAME = 'Homer'
        def INITIAL_LAST_NAME = 'Simpson'
        def INITIAL_ADDRESS0_NAME = 'me@asd.com'

        def entityUrl = '/people'

        given:
        def person = new Person(id: -1, firstName: INITIAL_FIRST_NAME, lastName: INITIAL_LAST_NAME)
        def emailAddress = new EmailAddress(parent: null, address: INITIAL_ADDRESS0_NAME, type: "H")
        person.emailAddresses << emailAddress
        repository.save(_ as Person) >> person

        when:
        def json = new JsonBuilder(person).toString()

        then:
        def returnFromPost = getMockMvc().perform(post(entityUrl)
                .content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())

        when:
        def personId = MockMvcHelper.getResponseMap(returnFromPost).id
        def urlOfCreatedRecord = entityUrl + '/' + personId
        repository.findOne(personId) >> person
        def returnFromGet = getMockMvc().perform(get(urlOfCreatedRecord)).andExpect(status().isOk())
        def responseMapFromGet = MockMvcHelper.getResponseMap(returnFromGet)
        def newPerson = new Person(responseMapFromGet)

        then: 'should get initial values'
        newPerson.firstName == INITIAL_FIRST_NAME
        newPerson.lastName == INITIAL_LAST_NAME
        newPerson.emailAddresses[0].address == INITIAL_ADDRESS0_NAME

        when: 'update a record'
        def newAddress = 'b@m.c'
        newPerson.emailAddresses[0].address = newAddress
        person.emailAddresses[0].address = newAddress // needed to update mock

        def returnFromPostForUpdate = getMockMvc().perform(post(entityUrl)
                .content(new JsonBuilder(newPerson).toString()).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
        def mapFromPostForUpdate = MockMvcHelper.getResponseMap(returnFromPostForUpdate)

        then: 'should get updated values'
        mapFromPostForUpdate.emailAddresses[0].address == newAddress

        when: 'delete a record'
        def deleteResponse = getMockMvc().perform(delete(urlOfCreatedRecord)).andExpect(status().isOk())
        // next line does not change the mock behavior, so test for record being deleted doesn't work for controller test
        repository.findOne(personId) >> null

        then: 'record should no longer exist'
        if (!mocked) {
            getMockMvc().perform(get(urlOfCreatedRecord)).andExpect(status().isNotFound())
        }

    }

    def boolean isMocked() {
        MockMvcHelper.isMocked(getRepository())
    }


}

