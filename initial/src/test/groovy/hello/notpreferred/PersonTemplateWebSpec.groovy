package hello.notpreferred

import groovy.json.JsonSlurper
import hello.Application
import hello.Person
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.TestRestTemplate
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.RestTemplate
import spock.lang.Ignore
import spock.lang.Specification

import javax.transaction.Transactional


@WebIntegrationTest(['server.port=0'])
@ContextConfiguration(loader = SpringApplicationContextLoader, classes = Application.class)
@Transactional
class PersonTemplateWebSpec extends Specification {

    RestTemplate template = new TestRestTemplate()
    def baseUrlWithoutPort = 'http://localhost'
    def baseUrl

    @Value('${local.server.port}')
    private int port

    def entityName = 'people' // used by PersonController. This returns no value from posts, which breaks this test.
    //def entityName = 'persons' // used by PersonRepository, which returns a value for posts, so test works fine.

    def urlOfCreatedRecord
    def breakTestToTestTransactions = false // this test fails when this is set to true

    // TODO: Move this method to an ancestor.
    def setup() {
        baseUrl = baseUrlWithoutPort + ':' + port
    }

    // unlike with PersonWebAppSpec, the next line does not cause inserted records to be rolled back
    // if the test doesn't end normally.
    @Transactional
    @Ignore // started failing when using mvc tests and populateParent
    def 'c.r.u.d person'() {
        def entityUrl = baseUrl + '/' + entityName  // use /persons to test PersonRepository, /people to test PersonController

        def INITIAL_FIRST_NAME = 'Homer'
        def INITIAL_LAST_NAME = 'Simpson'

        given: 'count number of pre-existing records'
// the next code fails while calling 'getAll' if any records in database are master/detail. this is due to a circular
//        reference in the json.
//        def numberOfExistingRecords = countNumberOfRecords(entityUrl)

        when: 'create a record'
        def person = new Person(firstName: INITIAL_FIRST_NAME, lastName: INITIAL_LAST_NAME)
        def responseFromPost = template.postForEntity(entityUrl, person, Person)

        then: 'should get valid return code and new record should be found in db'
        responseFromPost.statusCode == HttpStatus.CREATED
//        countNumberOfRecords(entityUrl) == numberOfExistingRecords + 1

        when: 'read a created record'
        person = responseFromPost.body
        urlOfCreatedRecord = entityUrl + '/' + person.id
        def returnFromGet = template.getForEntity(urlOfCreatedRecord, String.class)
        returnFromGet.statusCode == HttpStatus.OK

        then: 'should get initial values'
        def responseMapFromGet = getResponseMap(returnFromGet)
        responseMapFromGet.firstName == person.firstName
        responseMapFromGet.lastName == person.lastName
        if (breakTestToTestTransactions) {
            throw new Exception('stopping test to see if transaction rolled back')
        }

        when: 'update a record'
        def newFirstName = 'Bart'
        person.firstName = newFirstName

        template.postForEntity(entityUrl, person, Person)
        def returnFromGetAfterUpdate = template.getForEntity(urlOfCreatedRecord, String.class)

        then: 'should get updated values'
        def responseMapFromGetAfterUpdate = getResponseMap(returnFromGetAfterUpdate)
        responseMapFromGetAfterUpdate.firstName == person.firstName
        responseMapFromGetAfterUpdate.lastName == person.lastName

        when: 'delete a record'
        template.delete(urlOfCreatedRecord)

        then: 'record should no longer exist'
        def notFoundResult = template.getForEntity(urlOfCreatedRecord, String.class)
        notFoundResult.statusCode == HttpStatus.NOT_FOUND
    }

    private long countNumberOfRecords(entityUrl) {
        def body = template.getForEntity(entityUrl, String.class).body
        def map = new JsonSlurper().parseText(body)
        map.size
    }

// next method is needed, since test is not transactional even though @Transactional is used
    def cleanup() {
        if (!breakTestToTestTransactions) {
            if (urlOfCreatedRecord) {
                def result = template.getForEntity(urlOfCreatedRecord, String.class)
                if (result.statusCode != HttpStatus.NOT_FOUND) {
                    template.delete(urlOfCreatedRecord)
                }
                if (template.getForEntity(urlOfCreatedRecord, String.class).statusCode == HttpStatus.OK) {
                    throw new Exception('record found but should have been deleted')
                }
            }
        }
    }

    def getResponseMap(ResponseEntity response) {
        new JsonSlurper().parseText(response.body)
    }

}
