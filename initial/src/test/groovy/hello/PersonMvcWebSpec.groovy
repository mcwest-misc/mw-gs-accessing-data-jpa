package hello

import hello.helper.MockMvcHelper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

@WebIntegrationTest
@ContextConfiguration(loader = SpringApplicationContextLoader, classes = Application.class)
class PersonMvcWebSpec extends PersonControllerSpec {

    @Autowired
    WebApplicationContext wac

    @Autowired
    PersonRepository repository

    MockMvc mockMvc // despite the 'mock' name of this class, this test is in-container

    def setup() {
        mockMvc = MockMvcHelper.buildMockMvcFromWebAppContext(wac)
    }

}

/*
instructions:
  in setup, replace mockMvc with a web context based mockMvc
  autowire any services that are mocked in the ancestor, so non-mocked versions are tested here.
  in ancestor, refer to any instances of mockMvc using getMockMvc, so that it will get descendant (non-mocked) instance
 */
