package hello

import groovy.json.JsonBuilder
import hello.helper.MockMvcHelper
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class PersonControllerSpec extends Specification {

    MockMvc mockMvc

    PersonRepository repository

    def setup() {
        repository = Mock(PersonRepository)
        mockMvc = MockMvcBuilders.standaloneSetup(new PersonController(repository: repository)).build()
    }

    def 'c.r.u.d person'() {
        def entityUrl = '/people'

        given: 'count number of pre-existing records'
        repository.findAll() >> [new Person(emailAddresses: [new EmailAddress(address: 'foo@com.com')])]
        def numberOfExistingRecords = MockMvcHelper.countNumberOfRecords(entityUrl, getMockMvc())

        when: 'create'
        def INITIAL_FIRST_NAME = 'Homer'
        def INITIAL_LAST_NAME = 'Simpson'
        def person = new Person(id: 42, firstName: INITIAL_FIRST_NAME, lastName: INITIAL_LAST_NAME)
        repository.save(_ as Person) >> person
        def responseFromPost = getMockMvc().perform(post(entityUrl)
                .content(new JsonBuilder(person).toString()).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
        def personId = MockMvcHelper.getResponseMap(responseFromPost).id
        def urlOfCreatedRecord = entityUrl + '/' + personId
        repository.findOne(personId) >> person
        def returnFromGet = getMockMvc().perform(get(urlOfCreatedRecord)).andExpect(status().isOk())
        def responseMapFromGet = MockMvcHelper.getResponseMap(returnFromGet)
        def newPerson = new Person(responseMapFromGet)

        then: 'should be created in db with initial values'
        if (!mocked) { // can't test if mocked, since once we mock findAll don't know how to remock it
            MockMvcHelper.countNumberOfRecords(entityUrl, getMockMvc()) == numberOfExistingRecords + 1
        }
        newPerson.firstName == INITIAL_FIRST_NAME
        newPerson.lastName == INITIAL_LAST_NAME

        when: 'update a record'
        def newFirstName = 'Bart'
        newPerson.firstName = newFirstName
        person.firstName = newFirstName // can't do next line, since mock can't be pointed to a new instance of person
        //repository.findOne(personId) >> person
        def returnFromPostForUpdate = getMockMvc().perform(post(entityUrl)
                .content(new JsonBuilder(newPerson).toString()).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
        def mapFromPostForUpdate = MockMvcHelper.getResponseMap(returnFromPostForUpdate)

        then: 'should get updated values'
        mapFromPostForUpdate.firstName == newPerson.firstName
        mapFromPostForUpdate.lastName == newPerson.lastName

        when: 'delete a record'
        getMockMvc().perform(delete(urlOfCreatedRecord)).andExpect(status().isOk())
        // next line does not change the mock behavior, so test for record being deleted doesn't work for controller test
        repository.findOne(personId) >> null

        then: 'record should no longer exist'
        if (!mocked) {
            getMockMvc().perform(get(urlOfCreatedRecord)).andExpect(status().isNotFound())
        }
    }

    def boolean isMocked() {
        MockMvcHelper.isMocked(getRepository())
    }

}

