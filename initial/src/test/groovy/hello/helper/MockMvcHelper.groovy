package hello.helper

import groovy.json.JsonSlurper
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

class MockMvcHelper {
    static def getResponseMap(ResultActions resultFromAction) {
        new JsonSlurper().parseText(resultFromAction.andReturn().response.getContentAsString())
    }

    static MockMvc buildMockMvcFromWebAppContext(WebApplicationContext wac) {
        return MockMvcBuilders.webAppContextSetup(wac).build()
    }

    static long countNumberOfRecords(entityUrl, mockMvc) {
        def response = mockMvc.perform(MockMvcRequestBuilders.get(entityUrl)).andExpect(MockMvcResultMatchers.status().isOk())
        def map = MockMvcHelper.getResponseMap(response)
        map.size
    }

    static boolean isMocked(Object possibleMockObject) {
        possibleMockObject.toString().startsWith('Mock for type')
    }
}
