package hello.helper

import spock.lang.Specification


class NestedHelperSpec extends Specification {

        // TODO: pass in names of child properties, or do we just look for collections?

    def 'populate parent'() {

        when:
        def parent = new Parent([emailAddresses: [new Child(), new Child(), null]])
        NestedHelper.populateParent(parent)

        then:
        parent.emailAddresses[0].parent == parent
        parent.emailAddresses[1].parent == parent
        parent.emailAddresses[2] == null

        when:
        parent = new Parent()

        then:
        NestedHelper.populateParent(parent)
        parent.emailAddresses == null
        NestedHelper.populateParent(null) == null
    }

    def 'clear parent'() {

        when:
        def parent = new Parent([emailAddresses: [new Child(), new Child(), null]])
        NestedHelper.clearParent(parent)

        then:
        parent.emailAddresses[0].parent == null
        parent.emailAddresses[1].parent == null
        parent.emailAddresses[2] == null

        when:
        parent = new Parent()

        then:
        NestedHelper.clearParent(parent)
        parent.emailAddresses == null
        NestedHelper.clearParent(null) == null
    }

    def 'clear parent on a collection'() {

        given:
        def entity1 = new Parent([emailAddresses: [new Child(), new Child(), null]])
        def entity2 = new Parent([emailAddresses: [new Child(), new Child(), null]])
        List entities = [entity1, entity2]

        when:
        NestedHelper.clearParent(entities)

        then:
        entities[0].emailAddresses[0].parent == null
        entities[1].emailAddresses[0].parent == null
    }

    class Parent {
        Child[] emailAddresses
    }

    class Child {
        Parent parent
    }

}
