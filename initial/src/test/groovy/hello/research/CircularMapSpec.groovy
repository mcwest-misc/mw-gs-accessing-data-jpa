/**
 * a 'standalone' spring server test. Uses very similar code as the web app (in container) test.
 */

package hello.research

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.json.JsonBuilder
import spock.lang.Specification

class CircularMapSpec extends Specification {

    def 'circular map causes stackoverflow when serialized to json'() {

        boolean showError = false

        setup:
        def anAddress = new AnAddress(address: '363')
        def person = new APerson(addresses: [anAddress])
        if (showError) {
            // if any master/detail data exists in db, next line causes a stackoverflow due to circular reference in json
            anAddress.owner = person
        }
        def json = new JsonBuilder(person).toString()
        println json
    }

    class APerson {
        AnAddress[] addresses
    }

    class AnAddress {

        @JsonIgnore
        APerson owner

        String address
    }


}

