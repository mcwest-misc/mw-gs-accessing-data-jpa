/**
 * A database spec. Runs out of web container but interacts with database.
 */

package hello.research

import hello.Application
import hello.EmailAddress
import hello.Person
import hello.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import javax.transaction.Transactional

@IntegrationTest
@ContextConfiguration(loader = SpringApplicationContextLoader, classes = Application.class)
class PersonDbSpec extends Specification {

    @Autowired
    PersonRepository personRepository

    def person = new Person(firstName: "Homer", lastName: "Simpson")
    def emailAddress = new EmailAddress(address: "me@asd.com", type: "H")

    @Transactional
    def 'can insert master and detail in one phase even if keys are auto-generated'() {

        given: 'insert'
        person.emailAddresses << emailAddress

        when:
        person = personRepository.save person

        then: 'ids are populated'
        person.id
        person.emailAddresses[0].emailAddressNumber

        and: 'saved in database'
        def retrievedPerson = personRepository.findOne(person.id)
        retrievedPerson.id == person.id
        retrievedPerson.emailAddresses[0].emailAddressNumber == person.emailAddresses[0].emailAddressNumber
    }

// TODO: how to verify master and detail were deleted?
//    def cleanupSpec() {
// this code (which assumes we make static copy of repo in test instance method
// gives error: could not initialize proxy - no Session
//        !staticPersonRepository.getOne(newPersonId)
//    }
}