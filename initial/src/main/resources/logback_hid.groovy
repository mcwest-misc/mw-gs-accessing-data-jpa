import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.FileAppender

appender("FILE", FileAppender) {
    file = "~/tmp/log.sql"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
}

root(DEBUG, ["FILE"])


//<appender name="SQLROLLINGFILE">
//    <File>/tmp/sql.log</File>
//    <rollingPolicy>
//        <FileNamePattern>logFile.%d{yyyy-MM-dd}.log</FileNamePattern>
//    </rollingPolicy>
//    <layout>
//        <Pattern>%-4date | %msg %n</Pattern>
//    </layout>
//</appender>
//
//<logger name="org.hibernate.SQL" additivity="false">
//<level value="DEBUG"/>
//<appender-ref ref="SQLROLLINGFILE"/>
//</logger>
//
//<!--<logger name="org.hibernate.type" additivity="false">-->
//<!--<level value="TRACE"/>-->
//<!--<appender-ref ref="SQLROLLINGFILE"/>-->
//<!--</logger>-->