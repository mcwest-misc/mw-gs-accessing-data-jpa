package hello

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(exported=false)  // export this for research purposes, e.g. to see what returns values this returns
//@RepositoryRestResource(exported=true)  // the default
interface PersonRepository extends JpaRepository<Person, Long> {

//    List<Person> findByLastName(@Param("name") String name)

}