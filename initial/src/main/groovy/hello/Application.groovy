package hello

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class  Application implements CommandLineRunner {

    @Autowired
    CustomerRepository customerRepository

    static void main(String[] args) {
        SpringApplication.run Application
    }

    @Override
    void run(String... strings) throws Exception {

        // prevent database from filling up
        if (true) return

        // save a couple records
        customerRepository.save new Customer(firstName: "Jack", lastName: "Bauer")


        customerRepository.save new Customer(firstName: "Chloe", lastName: "O'Brian")
        customerRepository.save new Customer(firstName: "Kim", lastName: "Bauer")
        customerRepository.save new Customer(firstName: "David", lastName: "Palmer")
        customerRepository.save new Customer(firstName: "Michelle", lastName: "Dessler")
        lastName:
        // fetch all customers
        println "Customers found with findAll:"
        println "-------------------------------"
        for (Customer customer : customerRepository.findAll()) {
            println customer
        }
        println()

        // fetch an individual customer by ID
        Customer customer = customerRepository.findOne 415L
        println "Customer found with findOne 1L:"
        println "--------------------------------"
        println customer
        println()

        // fetch customers by last name
        println "Customer found with findByLastName 'Bauer':"
        println "--------------------------------------------"
        customerRepository.findByLastName("Bauer").each { Customer bauer ->
            println bauer
        }
    }

}