package hello.helper

class NestedHelper {

    // TODO: pass optional arg in that lists collection properties to process
    // or EVEN BETTER process all collection properties unless added to a passed-in exclude lsit
    static def populateParent(def parent) {
        parent?.emailAddresses.each { address ->
            address?.parent = parent
        }
        parent
    }

    static def clearParent(def parent) {
        parent?.emailAddresses.each { address ->
            address?.parent = null
        }
        parent
    }

    static def clearParent(Collection entities) {
        entities.each {entity ->
            clearParent(entity)
        }
    }
}
