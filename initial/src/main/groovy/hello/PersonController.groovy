package hello

import hello.helper.NestedHelper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Controller access to crud repository. This allows us to call methods on the crud repository that provide
 * full (master/detail for example) jpa support. The method called by the rest repository don't appear to support this.
 * note: this is at odds with the recommendation of some to never instantiate a crud repository
 * (e.g. http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-part-two-crud/)
 */
@RestController
@RequestMapping("/people")
class PersonController {

    @Autowired
    PersonRepository repository

    @RequestMapping(value = '', method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity post(@RequestBody Person person) {
        NestedHelper.populateParent(person)
        person = repository.save(person)
        NestedHelper.clearParent(person)
        return new ResponseEntity(person, HttpStatus.CREATED)
    }


    @RequestMapping(value = '', method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List getAll() {
        List all = repository.findAll()
        NestedHelper.clearParent(all)
    }

    @RequestMapping(value = '/{id}', method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity get(@PathVariable Long id) {
        def person = repository.findOne(id)
        if (person) {
            NestedHelper.clearParent(person)
            return new ResponseEntity(person, HttpStatus.OK)
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @RequestMapping(value = '/{id}', method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    // delete does not support passing a return value, odd because rest repository returns 204 or 404
    void delete(@PathVariable Long id) {
        repository.delete(id)
    }
}

