package hello

import javax.persistence.*

@Entity
@Table(name = "person")
class Person {

    @Id
    @GeneratedValue(generator = 'personId', strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = 'personId', sequenceName = 'PERSON_ID')
    Long id

    String firstName
    String lastName

    @OneToMany(mappedBy = 'parent', cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<EmailAddress> emailAddresses = new HashSet()

}

/* ddl

CREATE TABLE PERSON
(
   ID           NUMBER (*, 0),
   FIRST_NAME   VARCHAR2 (20),
   LAST_NAME    VARCHAR2 (20)
)

CREATE SEQUENCE PERSON_ID START WITH 25
                          INCREMENT BY 1
                          MAXVALUE 9999999999999999999999999999
                          NOMINVALUE
                          NOORDER
                          NOCYCLE
                          NOCACHE;

 */