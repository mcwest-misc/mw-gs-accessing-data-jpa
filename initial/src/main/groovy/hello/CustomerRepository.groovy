package hello

import org.springframework.data.jpa.repository.JpaRepository

//@RepositoryRestResource
interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName)
}