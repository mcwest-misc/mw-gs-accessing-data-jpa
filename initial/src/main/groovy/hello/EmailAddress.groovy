package hello

import javax.persistence.*

@Entity
@IdClass(EmailAddressId)
@Table(name = "person_email_address")
class EmailAddress implements Serializable{

    @Id
    @ManyToOne
    @JoinColumn(name = "person_id")
    Person parent

    @Id
    @GeneratedValue(generator = 'emailAddressNumber', strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = 'emailAddressNumber', sequenceName = 'EMAIL_ADDRESS_ID')
    @Column(name = "email_address_num")
    Long emailAddressNumber

    String address
    String type
}

/* ddl

CREATE TABLE PERSON_EMAIL_ADDRESS
(
   PERSON_ID           NUMBER (*, 0),
   EMAIL_ADDRESS_NUM   NUMBER (*, 0),
   ADDRESS             VARCHAR2 (100),
   TYPE                VARCHAR2 (2)
)

CREATE SEQUENCE EMAIL_ADDRESS_ID START WITH 14
                                 INCREMENT BY 1
                                 MAXVALUE 9999999999999999999999999999
                                 NOMINVALUE
                                 NOORDER
                                 NOCYCLE
                                 NOCACHE;

 */