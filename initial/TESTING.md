# Manual testing procedure 
(did this as automated testing was causing stackoverflow in getAll, has since been fixed) 

## Setup 
    grails bootrun
    (or simply use the team alias 'run')

## Tests 

### Create
    curl -i -X POST -H "Content-Type:application/json" -d '{  "firstName" : "Frodo",  "lastName" : "Baggins" }' http://localhost:8080/people

### Create master/detail
    curl -i -X POST -H "Content-Type:application/json" -d '{  "firstName" : "Frodo",  "lastName" : "Baggins", "emailAddresses": [{"address":"my@d.com", "type": "H"}]}' http://localhost:8080/people

### Read
    curl -i -X GET -H "Content-Type:application/json" http://localhost:8080/people

    curl -i -X GET -H "Content-Type:application/json" http://localhost:8080/people/580

### Update (note: these tests require you to first run the above create tests, then substitute the generated ids from the tests into the following curl commands) 
#### Parent works fine
    curl -i -XPOST -H "Content-Type:application/json" -d '{  "id": 661, "firstName" : "Frodo",  "lastName" : "Baggins2" }' http://localhost:8080/people
#### Parent child gives stackoverflow, why?
    curl -i -X POST -H "Content-Type:application/json" -d '{  "id": 1351, "firstName" : "Frodo",  "lastName" : "Baggins", "emailAddresses": [{"emailAddressNumber": 701, "address":"your@d.com", "type": "H"}]}' http://localhost:8080/people

### Delete 
    curl -i -X DELETE  http://localhost:8080/people/658