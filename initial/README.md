# Overview: 

# Prerequisites
* Oracle database server (see application.properties for connection info)

# Status: Mothballed
## Cannot update master/detail records
## For more info, see here: http://stackoverflow.com/questions/30468274/does-spring-jpa-support-updating-parent-child-data-having-composite-keys-using-o

## Evaluation Results
* Crud works for master (person), crd works for detail (email) but update causes a stackoverflow, happening somewhere in the jpa repo. How to solve? 

## Failing tests include: 
*  EmailAddressTemplateWebSpec (uses TestRestTemplate approach)
*  EmailAddressMvcWebSpec (uses MockMvc approach)
*  Manual tests (see below) 

# Manual testing procedure
See TESTING.md
